//
//  AGShoppingListViewController.h
//  Shopping cart
//
//  Created by Admin on 25/10/14.
//  Copyright (c) 2014 agaydash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGShoppingListViewController : UITableViewController<UITableViewDelegate, UITableViewDataSource>

@property NSArray *items;

@end
