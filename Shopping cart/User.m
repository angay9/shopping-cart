//
//  User.m
//  Shopping cart
//
//  Created by Admin on 23/10/14.
//  Copyright (c) 2014 agaydash. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic username;
@dynamic password;
@dynamic dateCreated;
@dynamic dateUpdated;

@end
