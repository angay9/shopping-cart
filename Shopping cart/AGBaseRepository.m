//
//  AGBaseRepository.m
//  Shopping cart
//
//  Created by Admin on 23/10/14.
//  Copyright (c) 2014 agaydash. All rights reserved.
//

#import "AGBaseRepository.h"
#import "AppDelegate.h"

@implementation AGBaseRepository

static NSManagedObjectContext *context;

+ (NSManagedObjectContext *) getContext {
    
    return [[[UIApplication sharedApplication] delegate] performSelector:@selector(managedObjectContext)];
}

@end
