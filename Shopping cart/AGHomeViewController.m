//
//  AGViewController.m
//  Shopping cart
//
//  Created by Admin on 23/10/14.
//  Copyright (c) 2014 agaydash. All rights reserved.
//

#import "AGHomeViewController.h"
#import "AGShoppingListViewController.h"

@interface AGHomeViewController ()

@end

@implementation AGHomeViewController

- (void)viewDidLoad
{
    //[AGUserRepository createUserWithUsername:@"angay9" andPassword:@"password"];
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Login segue
    if ([[segue identifier ] isEqualToString:@"loginSegue"]) {
        //AGShoppingListViewController *shoppingListVC = [segue destinationViewController];
    }
    
    // Register segue
    if ([[segue identifier] isEqualToString:@"reigsterSegue"]) {
        
    }
    
}

-(BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    // Login segue
    if ([identifier isEqualToString:@"loginSegue"]) {
        NSString *username = [[self usernameTextfield] text];
        NSString *password = [[self passwordTextfield] text];
        
        if ([AGUserRepository checkUserByUsername:username andPassword:password]) {
            return YES;
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No user with provided username/password found. Please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }

    }
    
    return YES;
    
}
- (IBAction)loginButtonClick:(id)sender {
    [self.view endEditing:YES];
}
- (IBAction)registerButtonClick:(id)sender {
}

@end
