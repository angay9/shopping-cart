//
//  AGViewController.h
//  Shopping cart
//
//  Created by Admin on 25/10/14.
//  Copyright (c) 2014 agaydash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGRegistrationViewController : UIViewController <UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *usernameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;
- (IBAction)registerButtonClick:(id)sender;

@end
