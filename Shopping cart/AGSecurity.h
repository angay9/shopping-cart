//
//  AGCrypter.h
//  Shopping cart
//
//  Created by Admin on 24/10/14.
//  Copyright (c) 2014 agaydash. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonHMAC.h>
@interface AGSecurity : NSObject
/*
 * Encrypts a string with sha1
 */
+ (NSString*) sha1:(NSString *)input;

+(BOOL) compareString:(NSString *) input1 toString: (NSString *) input2;
@end
