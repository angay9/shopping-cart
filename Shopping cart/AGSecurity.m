//
//  AGCrypter.m
//  Shopping cart
//
//  Created by Admin on 24/10/14.
//  Copyright (c) 2014 agaydash. All rights reserved.
//

#import "AGSecurity.h"

@implementation AGSecurity

+ (NSString *) sha1:(NSString *)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
    
}

+(BOOL) compareString:(NSString *) input1 toString: (NSString *) input2 {
    return [[AGSecurity sha1:input1] isEqualToString:input2];
}


@end
