//
//  AGViewController.m
//  Shopping cart
//
//  Created by Admin on 25/10/14.
//  Copyright (c) 2014 agaydash. All rights reserved.
//

#import "AGRegistrationViewController.h"

@interface AGRegistrationViewController ()

@end

@implementation AGRegistrationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Register a user
- (IBAction)registerButtonClick:(id)sender {
    NSString *username = [[self usernameTextfield] text];
    NSString *password = [[self passwordTextfield] text];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info" message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
    if ([AGUserRepository findUserByUserName:username]) {
        // User with this username already exists
        [alert setMessage:@"A user with this username already exists. Please try another username."];
        [alert show];
        return;
    }
    if ([AGUserRepository createUserWithUsername:username andPassword:password]) {
        [alert setMessage:@"Your account has been created succesfully."];
        [alert setTag:1];
        
    } else {
        [alert setMessage:@"An error occured. Please try again later"];
    }
    [alert show];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if ([alertView tag] == 1) {
        // User was registered.
        [self performSegueWithIdentifier:@"userHasRegisteredSegue" sender:nil];
        
    }
}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"userHasRegisteredSegue"]) {
        // User has registered succesfully. Redirect him to his office.
    }
}

@end
