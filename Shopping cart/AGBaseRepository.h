//
//  AGBaseRepository.h
//  Shopping cart
//
//  Created by Admin on 23/10/14.
//  Copyright (c) 2014 agaydash. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AGBaseRepository : NSObject
/*
 * Returns a context
 */
+ (NSManagedObjectContext *) getContext;

@end
