//
//  AGViewController.h
//  Shopping cart
//
//  Created by Admin on 23/10/14.
//  Copyright (c) 2014 agaydash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGHomeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *usernameTextfield;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (weak, nonatomic) IBOutlet UIButton *loginButtonClick;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;

- (IBAction)loginButtonClick:(id)sender;
- (IBAction)registerButtonClick:(id)sender;
@end
