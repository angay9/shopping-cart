//
//  AGUserRepository.h
//  Shopping cart
//
//  Created by Admin on 23/10/14.
//  Copyright (c) 2014 agaydash. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AGUserRepository : AGBaseRepository

/*
 * Gets a user by username
 */
+ (User *) findUserByUserName: (NSString *) username;


/*
 * Check if user exists
 */
+ (bool) checkUserByUsername: (NSString *)username andPassword: (NSString *) password;

/*
 * Create new user with provided username and password
 */
+ (User *) createUserWithUsername: (NSString *) username andPassword: (NSString *) password;

/*
 * Grab all users
 */
+ (NSArray *) allUsers;

@end
