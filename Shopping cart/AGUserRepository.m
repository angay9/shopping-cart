//
//  AGUserRepository.m
//  Shopping cart
//
//  Created by Admin on 23/10/14.
//  Copyright (c) 2014 agaydash. All rights reserved.
//

#import "AGUserRepository.h"

@implementation AGUserRepository

#pragma mark - getUserByUserName
+ (User *) findUserByUserName:(NSString *) username {
    @try {
        NSManagedObjectContext *context = [AGBaseRepository getContext];
        NSError *error = nil;
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"User"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"username == %@", username];
        [request setPredicate:predicate];
        
        NSArray *users = [context executeFetchRequest:request error:&error];
        if (users != nil) {
            return [users firstObject];
        }
        return nil;
    }
    @catch (NSException *exception) {
        
        NSLog(@"Exception: %@", [exception reason]);
        return nil;
    }
    
}


+ (bool) checkUserByUsername: (NSString *)username andPassword: (NSString *) password {
    @try {
        NSManagedObjectContext *context = [AGBaseRepository getContext];
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"User"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"username == %@", username];
        NSError *error = nil;
        [request setPredicate: predicate];
        User *user = [[context executeFetchRequest:request error:&error] firstObject];
        
        if (user) {
            //NSString *hashedPassword = [AGSecurity sha1: [user password]];
            //return [AGSecurity compareString:password toString:hashedPassword];
            NSLog(@"%@", [user password]);
            return [[user password] isEqualToString:password];
        }
        return NO;
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@", [exception reason]);
        return NO;
    }
    
}

+ (User *) createUserWithUsername: (NSString *) username andPassword: (NSString *) password {
    @try {
        NSManagedObjectContext *context = [AGBaseRepository getContext];
        
        User *user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
        [user setUsername:username];
        //[user setPassword:[AGSecurity sha1:password]];
        [user setPassword:password];
        [user setDateCreated:[NSDate date]];
        [user setDateUpdated:[NSDate date]];
        [context insertObject:user];
        NSError *error;
        [context save:&error];
        return user;
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@", [exception reason]);
        return nil;
    }
}

+ (NSArray *) allUsers {
    @try {
        NSManagedObjectContext *context = [AGBaseRepository getContext];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *user = [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
        [request setEntity:user];
        NSError *error = nil;
        NSArray *users = [context executeFetchRequest:request error:&error];
        NSLog(@"%@", users);
        return users;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@", [exception reason]);
        return [[NSArray alloc] init];
    }
}

@end
